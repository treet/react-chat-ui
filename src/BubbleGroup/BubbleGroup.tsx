import React from 'react';

import { ChatBubble as DefaultChatBubble, ChatBubbleProps } from '../ChatBubble/ChatBubble';
import { Message } from '../types';

import styles from './styles';

export type BubbleGroupProps = {
  messages: Message[];
  id: string | number;
  showSenderName?: boolean;
  chatBubble?: React.ComponentClass<ChatBubbleProps> | React.FunctionComponent<ChatBubbleProps>;
  bubbleStyles?: {
    userBubble?: React.CSSProperties;
    chatbubble?: React.CSSProperties;
    text?: React.CSSProperties;
  };
  bubblesCentered?: boolean;
  senderName?: string;
};

export const BubbleGroup: React.FunctionComponent<BubbleGroupProps> = props => {
  const { bubblesCentered, bubbleStyles, showSenderName, chatBubble, senderName, messages } = props;
  const ChatBubble = chatBubble || DefaultChatBubble;
  const sampleMessage = messages[0];

  const messageNodes = messages.map((message, i) => {
    return <ChatBubble key={i} message={message} bubblesCentered={bubblesCentered} bubbleStyles={bubbleStyles} />;
  });

  return (
    <div style={styles.chatbubbleWrapper}>
      {showSenderName && (senderName || sampleMessage.senderName) !== '' && sampleMessage.id !== 0 && (
        <h5 style={styles.bubbleGroupHeader}>{senderName || sampleMessage.senderName}</h5>
      )}
      {messageNodes}
    </div>
  );
};
