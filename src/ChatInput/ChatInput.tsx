import React from 'react';

type ChatInputStyles = {
  chatInput: React.CSSProperties;
  inputStyle: React.CSSProperties;
};

const styles: ChatInputStyles = {
  chatInput: {
    flex: 1,
  },
  inputStyle: {
    border: 'none',
    borderTopWidth: 1,
    borderTopStyle: 'solid',
    borderTopColor: '#ddd',
    fontSize: 16,
    outline: 'none',
    padding: 30,
    width: '100%',
  },
};

export type ChatInputProps = {
  inputStyles?: React.CSSProperties;
  inputPlaceholder?: string;
};

export const ChatInput: React.FunctionComponent<ChatInputProps> = props => {
  const { inputStyles, inputPlaceholder } = props;

  return (
    <div className="chat-input" style={styles.chatInput}>
      <input type="text" style={inputStyles || styles.inputStyle} placeholder={inputPlaceholder} />
    </div>
  );
};
