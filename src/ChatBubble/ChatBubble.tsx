import React from 'react';

import { Message } from '../types';

import styles from './styles';

const defaultBubbleStyles = {
  userBubble: {},
  chatbubble: {},
  text: {},
};

export type ChatBubbleProps = {
  message: Message;
  bubbleStyles?: {
    userBubble?: React.CSSProperties;
    chatbubble?: React.CSSProperties;
    text?: React.CSSProperties;
  };
  bubblesCentered?: boolean;
};

export const ChatBubble: React.FunctionComponent<ChatBubbleProps> = props => {
  const { bubblesCentered, message } = props;
  let { bubbleStyles } = props;
  const { userBubble, chatbubble, text } = bubbleStyles || defaultBubbleStyles;
  // message.id 0 is reserved for blue
  const chatBubbleStyles: React.CSSProperties =
    message.id === 0
      ? {
          ...styles.chatbubble,
          ...(bubblesCentered ? {} : styles.chatbubbleOrientationNormal),
          ...chatbubble,
          ...userBubble,
        }
      : {
          ...styles.chatbubble,
          ...styles.recipientChatbubble,
          ...(bubblesCentered ? {} : styles.recipientChatbubbleOrientationNormal),
          ...chatbubble,
          ...userBubble,
        };

  return (
    <div style={{ ...styles.chatbubbleWrapper }}>
      <div style={chatBubbleStyles}>
        <p style={{ ...styles.p, ...text }}>{message.message}</p>
      </div>
    </div>
  );
};
