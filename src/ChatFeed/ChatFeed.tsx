// Copyright 2017-2020 Brandon Mowat
// Copyright 2020 Treet
// Written, developed, and designed by Brandon Mowat for the purpose of helping
// other developers make chat interfaces.

import React from 'react';

import { BubbleGroup } from '../BubbleGroup';
import { ChatBubble as DefaultChatBubble, ChatBubbleProps } from '../ChatBubble';
import { ChatInput } from '../ChatInput';
import { Message } from '../types';

import styles from './styles';

// Model for ChatFeed props.
export type ChatFeedProps = {
  bubblesCentered?: boolean;
  bubbleStyles?: object;
  hasInputField?: boolean;
  isTyping?: boolean;
  maxHeight?: number;
  messages: Message[];
  showSenderName?: boolean;
  chatBubble?: React.ComponentClass<ChatBubbleProps> | React.FunctionComponent<ChatBubbleProps>;
};

/**
 * Determines what type of message/messages to render.
 */
const renderMessages = (
  messages: Message[],
  props: Pick<ChatFeedProps, 'isTyping' | 'bubbleStyles' | 'chatBubble' | 'showSenderName'>,
): React.ReactNode => {
  const { isTyping, bubbleStyles, chatBubble, showSenderName } = props;
  const ChatBubble = chatBubble || DefaultChatBubble;
  let group: Message[] = [];
  const messageNodes = messages.map((message, index) => {
    group.push(message);
    // Find diff in message type or no more messages
    if (index === messages.length - 1 || messages[index + 1].id !== message.id) {
      const messageGroup = group;

      group = [];

      return (
        <BubbleGroup
          key={index}
          messages={messageGroup}
          id={message.id}
          showSenderName={showSenderName}
          chatBubble={ChatBubble}
          bubbleStyles={bubbleStyles}
        />
      );
    }

    return null;
  });

  // Other end is typing...
  if (isTyping) {
    messageNodes.push(
      <div key="isTyping" style={{ ...styles.chatbubbleWrapper }}>
        <ChatBubble message={{ id: 1, message: '...', senderName: '' }} bubbleStyles={bubbleStyles} />
      </div>,
    );
  }

  // return nodes
  return messageNodes;
};

export const ChatFeed: React.FunctionComponent<ChatFeedProps> = props => {
  const messagesEndRef = React.createRef<HTMLDivElement>();
  const { maxHeight, messages } = props;

  React.useEffect(() => {
    if (messages.length > 0 && messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView();
    }
  }, [messages, messagesEndRef]);

  return (
    <div id="chat-panel" style={styles.chatPanel}>
      <div className="chat-history" style={{ ...styles.chatHistory, maxHeight }}>
        <div className="chat-messages">{renderMessages(messages, props)}</div>
        <div ref={messagesEndRef} />
      </div>
      {props.hasInputField && <ChatInput />}
    </div>
  );
};
