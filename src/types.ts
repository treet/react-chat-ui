/**
 * Representation of message data in react-chat-ui library.
 */
export type Message = {
  /**
   * The `id` of a message is a number used to identify which user created the
   * message. It's used for grouping messages together and determining the
   * bubble colour. This will be renamed in the future.
   */
  id: string | number;
  /**
   * This is the actual text that will be displayed in the bubble.
   */
  message: string;
  /**
   * Sender name is an optional parameter that associates a name to a message
   * (can be thought of as a string representation of an `id`, although your
   * id's will be unique, users may have the same name).
   */
  senderName?: string;
  /**
   * Optional timestamp when the message was sent.
   */
  timestamp?: number | Date;
};
