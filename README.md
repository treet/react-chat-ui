# 🙊 @treet/react-chat-ui

A library of React components for building chat UI's. This is an fork of the
original [React Chat UI](https://github.com/brandonmowat/react-chat-ui) maintained
by [Treet](https://treet.fi) developers.

## Features

- Auto scroll to bottom
- SUPER easy to use
- Multiple user grouping (try it out in the demo)

Keep in mind that this project is still in the early stages of development. If
you encounter a bug or have a feature request, please create an issue.

## Installation

```bash
$ yarn add @treet/react-chat-ui
```

## Basic Usage

```javascript
import { ChatFeed, Message } from '@treet/react-chat-ui'

// Your code stuff...

render() {

  return (

    // Your JSX...

    <ChatFeed
      messages={this.state.messages} // Boolean: list of message objects
      isTyping={this.state.is_typing} // Boolean: is the recipient typing
      hasInputField={false} // Boolean: use our input, or use your own
      showSenderName // show the name of the user who sent the message
      bubblesCentered={false} //Boolean should the bubbles be centered in the feed?
      // JSON: Custom bubble styles
      bubbleStyles={
        {
          text: {
            fontSize: 30
          },
          chatbubble: {
            borderRadius: 70,
            padding: 40
          }
        }
      }
    />

    // Your JSX...

  )

}
```

Make sure to keep a list of proper message objects in your class state.
Like so:

```javascript
//...
this.state = {
  messages: [
    {
      id: 1,
      message: "I'm the recipient! (The person you're talking to)",
    }, // Gray bubble
    {
      id: 0,
      message: "I'm you -- the blue bubble!",
    }, // Blue bubble
  ],
  //...
};
//...
```

## API

- [ChatFeed](./src/ChatFeed)
- [ChatBubble](./src/ChatBubble)
- [BubbleGroup](./src/BubbleGroup)

## Contributing!¡1 🔧

Contributions are always welcomed and encouraged.

## TODO

- documentation
- documentation
- documentation

## Development

```bash
$ yarn install && yarn watch
```
